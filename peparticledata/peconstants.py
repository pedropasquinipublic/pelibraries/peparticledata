#########################################################
### 				created by						  ### 
###				Pedro Pasquini						  ###
###		https://inspirehep.net/authors/1467863		  ###
### 												  ### 
### 				creation date					  ### 
### 				24th Jan 2024					  ### 
#########################################################
from numpy import sqrt



## see https://pdg.lbl.gov/2023/reviews/contents_sports.html


############################
#### physical constants ####
############################

c_m_per_s    = 2.99792458e8
hbar_MeV_s   = 6.582119569e-22
hbarc_MeV_fm = 197.3269804
e_Coulomb    = 1.602176634e-19
alpha        = 1/137.035999084
GF_perGeV2   = 1.1663788e-5
GF_pereV2    = 1.1663788e-23
GN_eVm2      = 6.70883e-57  
M_plank_eV   = sqrt(1.0/GN_eVm2)
kB_eV_perK   = 8.6173324e-5

############################
###### particle masses #####
############################

#######################
### Charged Leptons ###
#######################
me_eV   = 0.510998950e6 
mmu_eV  = 105.6583755e6 
mtau_eV = 1.77686e9 

################
###  Quarks  ###
################
## u-like quarks 
mu_eV =   2.16e6
mc_eV =   1.27e9
mt_eV = 172.69e9

## d-like quarks 
md_eV =  4.67e6
ms_eV = 93.40e6
mb_eV =  4.18e9

#######################
###  Gauge Bosons  ####
#######################
mw_eV = 80.377e9
mz_eV = 91.1876e9

cos_thw = mw_eV/mz_eV
sin_thw = sqrt(1 - cos_thw*cos_thw)

################
###  Higgs  ####
################
mh_eV   = 125.25e9
hvev_eV = 1/sqrt(sqrt(2)*GF_pereV2)

###############
### Hadrons ###
###############

## Mesons ## 
mpi0_eV  =  134.9768e6
mpip_eV  =  139.57039e6
mpim_eV  =  mpip_eV
mk0_eV   =  497.611e6
mkp_eV   =  493.677e6
mkm_eV   =  493.677e6
meta_eV  =  547.862e6
mDs_eV   = 1968.30e6
metac_eV = 2984.1e6
metab_eV = 9398.7e6

## Barions ##
mp_eV = 938.27208816e6
mn_eV = 939.56542052e6

###########################
### Coupling constants  ###
###########################

## SU(2) coupling constant
gSU2  = sqrt(8*GF_pereV2*(mw_eV**2)/sqrt(2))