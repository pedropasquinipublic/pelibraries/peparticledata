#########################################################
### 				created by						  ### 
###				Pedro Pasquini						  ###
###		https://inspirehep.net/authors/1467863		  ###
### 												  ### 
### 				creation date					  ### 
### 				06th Oct 2023					  ### 
#########################################################

from peparticledata.peconstants import c_m_per_s, hbarc_MeV_fm, hbar_MeV_s

###################################
#### distance only conversions ####
###################################

km_to_m  = 1.0e3
m_to_cm  = 1.0e2
km_to_cm = km_to_m*m_to_cm
m_to_fm  = 1.0e15

m_to_km  = 1/km_to_m
m_to_cm  = 1/m_to_cm
cm_to_km = 1/km_to_cm
fm_to_m  = 1/m_to_fm

#############################
### Area only conversions ###
#############################

m2_to_cm2 = m_to_cm*m_to_cm
cm2_to_m2 = 1/m2_to_cm2

barn_to_m2  = 1.0e-28
m2_to_barn  = 1/barn_to_m2

barn_to_cm2 = barn_to_m2*m2_to_cm2
cm2_to_barn = barn_to_cm2


#################################
#### energy only conversions ####
#################################

eV_to_keV = 1.0e-3
eV_to_MeV = 1.0e-6
eV_to_GeV = 1.0e-9
eV_to_TeV = 1.0e-12

keV_to_eV = 1/eV_to_keV
MeV_to_eV = 1/eV_to_MeV
GeV_to_eV = 1/eV_to_GeV
TeV_to_eV = 1/eV_to_TeV

MeV_to_GeV = 1.0e-3
GeV_to_MeV = 1/MeV_to_GeV


################################
### Natural units conversion ###
################################

m_to_s = c_m_per_s 

m_to_MeV_minus1  = 1/(hbarc_MeV_fm*fm_to_m)
MeV_to_m_minus1  = m_to_MeV_minus1

m_to_eV_minus1  = 1/(hbarc_MeV_fm*fm_to_m*MeV_to_eV)
eV_to_m_minus1  = m_to_eV_minus1

s_to_eV_minus1  = 1/(hbar_MeV_s*MeV_to_eV)
eV_to_s_minus1  = 1/(hbar_MeV_s*MeV_to_eV)

s_to_MeV_minus1  = 1/(hbar_MeV_s)
MeV_to_s_minus1  = 1/(hbar_MeV_s)

barn_to_MeV_minus2 = barn_to_m2*m_to_MeV_minus1*m_to_MeV_minus1
barn_to_GeV_minus2 = barn_to_m2*m_to_MeV_minus1*m_to_MeV_minus1/MeV_to_GeV/MeV_to_GeV

Mpcs_to_eV_minus1  = ((1e6)*(3.08567758e16)*m_to_eV_minus1)
eV_to_Mpcs_minus1  = Mpcs_to_eV_minus1**-1  

km_persec_perMpc_to_eV = km_to_m*m_to_eV_minus1/s_to_eV_minus1/Mpcs_to_eV_minus1