#########################################################
### 				created by						  ### 
###				Pedro Pasquini						  ###
###		https://inspirehep.net/authors/1467863		  ###
### 												  ### 
### 				creation date					  ### 
### 				26th Jan 2024					  ### 
#########################################################

import peparticledata.peconstants as peconstants

class ParticleInfo():
    name    : str 
    latex   : str 
    mass    : float 
    charge  : float
    pdgCode : int

    def __init__(self, inName: str, inLatex: str, inMass: float, inCharge, inPdg: int):
        self.name    = inName
        self.latex   = inLatex
        self.mass    = inMass
        self.charge  = inCharge
        self.pdgCode = inPdg

    def __str__(self) -> str:
        __return_string = self.name + "\n\tLaTeX symbol: "+f"{self.latex}\n\t" + f"mass: {self.mass:.4e} eV\n\tcharge: {self.charge:.2f}\n\tPDG code: {self.pdgCode}\n"
        return __return_string

SMInfo = {
                        #######################
                        ### Charged Leptons ###
                        #######################
                        "e"     : ParticleInfo("electron", r"e"    , peconstants.me_eV  , -1.0, 11),
                        "mu"    : ParticleInfo("muon"    , r"\mu"  , peconstants.mmu_eV  , -1.0, 13),
                        "tau"   : ParticleInfo("tau"     , r"\tau" , peconstants.mtau_eV, -1.0, 15),

                        ################
                        ###  Quarks  ###
                        ################
                        ## u-like quarks 
                        "u"     : ParticleInfo("up"    , r"u"   , peconstants.mu_eV, 2.0/3, 2),
                        "c"     : ParticleInfo("charm" , r"c"   , peconstants.mc_eV, 2.0/3, 4),
                        "t"     : ParticleInfo("top"   , r"t"   , peconstants.mt_eV, 2.0/3, 6),

                        ## d-like quarks 
                        "d"     : ParticleInfo("down"    , r"d"   , peconstants.md_eV, -1.0/3, 1),
                        "s"     : ParticleInfo("strange" ,  r"s"   , peconstants.ms_eV, -1.0/3, 3),
                        "b"     : ParticleInfo("botton"  , r"b"   , peconstants.mb_eV, -1.0/3, 5),

                        #######################
                        ###  Gauge Bosons  ####
                        #######################,
                        "gamma"  : ParticleInfo("photon" , r"\gamma" , 0.0         , 0  ,  22),
                        "g"      : ParticleInfo("gluon"  , r"g"      , 0.0         , 0  ,  21),
                        "w+"     : ParticleInfo("W-plus" , r"W^+"    , peconstants.mw_eV, 1.0,  24),
                        "w-"     : ParticleInfo("W-minus", r"W^-"    , peconstants.mw_eV, 1.0, -24),
                        "z"      : ParticleInfo("Z-zero" , r"Z^0"    , peconstants.mz_eV, 0  ,  23),

                        ################
                        ###  Higgs  ####
                        ################
                        "h"     : ParticleInfo("Higgs"   , r"h"       , peconstants.mh_eV, 0, 25),

                        ###############
                        ### Hadrons ###
                        ###############
                        ## Mesons ## 
                        "pion0" : ParticleInfo("pion-0"    , r"\pi^0"  , peconstants.mpi0_eV ,    0,  111),
                        "pion+" : ParticleInfo("pion-plus" , r"\pi^+"  , peconstants.mpip_eV ,  1.0,  211),
                        "pion-" : ParticleInfo("pion-minus", r"\pi^-"  , peconstants.mpim_eV , -1.0, -211),
                        "kaon0" : ParticleInfo("kaon-0"    , r"K^0"    , peconstants.mk0_eV  ,    0,  311),
                        "kaon+" : ParticleInfo("kaon-plus" , r"K^+"    , peconstants.mkp_eV  ,  1.0,  321),
                        "kaon-" : ParticleInfo("kaon-minus", r"K^-"    , peconstants.mkm_eV  , -1.0, -321),
                        "eta0"  : ParticleInfo("eta"       , r"\eta^0" , peconstants.meta_eV ,    0,  221),
                        "Ds"    : ParticleInfo("Ds"        , r"D_s"    , peconstants.mDs_eV  ,    0,  431),
                        "etac"  : ParticleInfo("eta-c"     , r"\eta_c" , peconstants.metac_eV,    0,  441),
                        "etab"  : ParticleInfo("eta-b"     , r"\eta_b" , peconstants.metab_eV,    0,  551),



                        ## Barions ##
                        "p" : ParticleInfo("proton"  , r"p"  , peconstants.mp_eV, 1.0, 2212),
                        "n" : ParticleInfo("neutron" , r"n"  , peconstants.mn_eV, 0.0, 2112)
                       }



