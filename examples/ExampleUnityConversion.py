
import peparticledata.peunit as phys_unity

if __name__ == "__main__":
	

  print(f"Sanity check: c in natural units:{phys_unity.c_m_per_s*phys_unity.m_to_MeV_minus1/phys_unity.s_to_MeV_minus1}")


  print("Other Examples:")

  tau_pi0_s = 8.5e-17

  print(f"pi0 lifetime is {tau_pi0_s:.3e} s or {tau_pi0_s*phys_unity.s_to_MeV_minus1/phys_unity.MeV_to_GeV:.3e}"+r" GeV^{-1}") 

  print(f"pi0 decay width is {1/tau_pi0_s:.3e} s^{-1} or {((1/tau_pi0_s)/phys_unity.s_to_MeV_minus1)*phys_unity.MeV_to_GeV:.3e}"+r" GeV") 

