
from peparticledata.peparticle import SMInfo as The_Standard_Model

if __name__ == "__main__":
	

    print("These are (some of) the particles in the Standard Model\n\n")
    print("------------------------------------------------------------")
    for _, particle in The_Standard_Model.items():
        print(particle)

    print("------------------------------------------------------------")