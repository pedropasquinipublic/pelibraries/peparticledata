from setuptools import setup


setup(
    name='peparticledata',
    version='1.0.0',    
    description='Some usefull constants to convert physical units',
    url='https://gitlab.com/pedropasquinipublic/pelibraries/peparticledata',
    author='Pedro Pasquini',
    author_email='pedrosimpas@gmail.com',
    packages=['peparticledata'],
    classifiers=[
        'Intended Audience :: Science/Research', 
        'Operating System :: POSIX :: Linux',      
        'Programming Language :: Python :: 3.8',
    ],
)
