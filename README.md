________________________________________________
<div align="center">
peparticledata

A python library with particle information from the PDG

Creation date: 26th January 2024          
Created by 

Pedro Pasquini      
<https:/inspirehep.net/authors/1467863>    
</div>

________________________________________________

# Introduction
This file contains some particle information, physical constants and 
convertion constants to easely transform units. Should be updated in the future to 
accomodate more interesting convertion factors and particles. All the information
was obtained from the PDG [1].

# Instalation

To **install** you can use pip. 

Go to folder in terminal and run 
>> pip3 install -e ../peparticledata

To **uninstall** it just type
>> pip3 uninstall peparticledata

# Usage 
Should be very simple to use. See examples in ./examples. Also, check 
inside the files peconstants.py, peparticle.py, and peunit.py for all the possibilities.

# References

[1] R. L. Workman _et al._ [Particle Data Group]
''<em>Review of Particle Physics</em>'',
<a href="http://dx.doi.org/10.1093/ptep/ptac097">
PTEP 2022, 083C01 (2022)</a>
see also the <a href="https://pdg.lbl.gov/">
PDG website</a>.
